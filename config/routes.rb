ClubBiz::Application.routes.draw do
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  root to: 'users#index'

  resources :comments, :only => [:create]

  match '/index_page', to: 'users#index', via: 'get'

  match '/user_login', to: 'users#login', via: 'post'

  match '/user_logout', to: 'users#logout', via: 'get'

  match '/user_events', to: 'users#show_events', via: 'get'

  match '/create_flag', to: 'users#create_flag', via: 'post'

  match '/destroy_flag', to: 'users#destroy_flag', via: 'delete'

  match '/society_index', to: 'societies#index', via: 'get'

  resource :societies, only: [:new, :create, :update]

  match 'society/:id', to: 'societies#show', via: 'get'

  match '/society_events', to: 'societies#show_events', via: 'get'

  resource :events, only: [:new, :create, :update]

  match '/event_image_upload', to: 'events#event_image_upload', via: [:get, :post]

  match '/event_index', to: 'events#show', via: 'get'

  match '/event_edit', to: 'events#edit', via: 'get'

  match '/event_report', to: 'events#report', via: 'get'

  match '/event_create', to: 'events#create', via: 'post'

  match 'event/:id', to: 'events#show', via: 'get'

  match '/event_show_comments', to: 'events#show_comments', via: 'get'

  match '/search', to: 'users#search', via: 'get'

  match '/ticket_request', to: 'tickets#request_tickets', via: 'get'

  match '/ticket_reserve', to: 'tickets#reserve', via: 'post'

  if Rails.env.production?
    get '404', :to => 'application#page_not_found'
    get '422', :to => 'application#server_error'
    get '500', :to => 'application#server_error'
  end


  match '/my_tickets', to: 'users#show_tickets', via: 'get'
  # match '/show_user_societies', to: 'domain_router#show_user_societes', via: 'get'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
