class UsersController < ApplicationController
  before_filter :set_services
  skip_before_action :verify_authenticity_token, only: :login
  skip_before_action :require_login, only: [:login, :index]

  # Create a new User, just to record the login or member user
  # This method should not be used to create a new user
  # @param params{name, email, portrait_link, institution_id}
  # @return
  def create

  end

  # The index page of user, may be login page or current user's home page,
  # The home page should contain the events which the user is interested in. This includes:
  # • The events that current user likes.
  # • The events that belong to the society that the user follows.
  # • The events that belong to the society that the user is a member of.
  # @param current_user
  # @return HTML
  def index
    if current_user
      events = []

      following_societies = @user_service.find_user_following_societies current_user.id
      following_societies.each do |society|
        events << @society_service.find_society_events(society.id)
      end

      joining_societies = @user_service.find_user_joining_societies current_user.id
      joining_societies.each do |society|
        events << @society_service.find_society_events(society.id)
      end

      events << @user_service.find_user_liking_events(current_user.id)

      events.flatten!

      events.reject! { |event| event.time < Time.now }

      events.uniq! {|event| event.id}

      events.sort_by! &:time    # !!!!!!INTERESTING!!!!!

      @timeline_items = events

      # sidebar

      session[:societies] = []
      joining_societies.each do |society|
        session[:societies].push({id: society.id, image: @society_service.find_society_image(society.id).first.link})
      end

      render 'users/index'
    else
      render 'users/login', layout: 'login_layout'
    end
  end

  # The login response. Should: (DEVISE)
  # • Redirect to external link based on the institution choice.
  # • Get the feedback from external link.
  # • Redirect to the index page if success.
  #   Show error if not success.
  # • Store the information of user if the first time login.
  # @param institution_id
  # @return index_page
  def login
    oauth_link = @society_service.find_institution(login_params[:institution]).auth_link

    user = @user_service.user_login_validation oauth_link, login_params[:email], login_params[:password]

    if user.blank?
      redirect_to root_url
    else
      rnt = @user_service.find_or_create_user user['name'], user['email'], login_params[:institution]
      session[:current_user_id] = rnt.id
      redirect_to root_url
    end
  end

  # Delete one of the user. To be specified.
  def destroy

  end

  # The logout response of current user(authentication). Should: (DEVISE)
  # • Log the user out.
  # • Redirect to the index page(login page).
  # @param
  # @return login_page
  def logout
    @_current_user = session[:current_user_id] = nil
    session[:societies] = nil
    redirect_to root_url
  end

  # The search function for current user(authentication). The results may contain:
  # • The events which meet the keyword.
  # • The societies which meet the keyword.
  # • The venues which meed the keyword.
  # This method should also require the filter states in order
  # to filter the results.
  # @param keyword
  # @return search_result_page
  def search
    keyword = params[:keyword]
    results = Hash.new
    results[:events] = []
    results[:societies] = []
    if params[:event]
      results[:events] += @event_service.find_events_by_name(keyword)
    end

    if params[:venue]
      results[:events] += @event_service.find_events_by_venue(keyword)
    end

    if params[:society]
      results[:societies] += @society_service.find_societies_by_name(keyword)
    end

    if params[:society_events]
      results[:societies].each do |society|
        results[:events] += @society_service.find_society_events society.id
      end
    end

    results[:events].flatten!

    results[:events].uniq! {|event| event.id}

    @results = results

    respond_to do |format|
      format.html { render :layout => false }
      format.json { render json: {results: @results} }
    end

  end

  # Show the societies of current user. These may contain:
  # • The societies which current user is a member of.
  # • The societies which current user follows.
  # @param current_user
  # @return user_societie_page
  def show_societies

  end

  # Show the events of current user. These may contain:
  # • The events which current user likes.
  # • The events which current user bought a ticket for.
  # @param current_user
  # @return user_event_page
  def show_events
    liked_events = @user_service.find_user_liking_events current_user.id
    booked_events = @user_service.find_user_booked_events current_user.id

    render 'users/show_events', locals: {liked_events: liked_events, booked_events: booked_events}
  end

  # Show the notifications of current user. These may contain:
  # • The notifications sent by system.
  # • The notifications sent because of society approval.
  # • The notifications sent because of membership added.
  # • The notifications sent because of ticket buying.
  # However, this method do not need to care about these...
  # @param current_user
  # @return user_notification_page
  def show_notifications

  end

  # Show the tickets of current user. These may contain:
  # • The tickets which current user reversed but not bought.
  # • The tickets which current user bought.
  # @param current_user
  # @return user_ticket_page
  def show_tickets


    user_ticket = @user_service.find_user_tickets(current_user.id)

    @user_tickets = user_ticket

    render 'users/my_tickets'
  end

  # Update the portrait of the current user.
  # @param current_user, new_portrait_link
  # @return new_portrait : JSON
  def update_portrait

  end

  # Response to the "Follow" action.
  # This method should create a follow relation. And update the
  # "Follow" button to "Unfollow".
  # @param current_user, society_id
  # @return JSON/HTML
  def create_flag
    @flag = flag_params.merge(state: true)
    if  @flag[:type]== 'society'
      @user_service.create_follow current_user.id, @flag[:id]
    elsif @flag[:type] == 'event'
      @user_service.create_like current_user.id, @flag[:id]
    else
      flash[:error] = 'Wrong Action'
    end

    render 'flaggables/flag'
  end

  # Response to the "Unfollow" action.
  # This method should destroy a follow relation. And update the
  # "Unfollow" button to "Follow"
  # @param current_user, society_id
  # @return JSON/HTML
  def destroy_flag
    @flag = flag_params.merge(state: false)
    if  @flag[:type]== 'society'
      @user_service.destroy_follow current_user.id, @flag[:id]
    elsif @flag[:type] == 'event'
      @user_service.destroy_like current_user.id, @flag[:id]
    else
      flash[:error] = 'Wrong Action'
    end
    render 'flaggables/flag'
  end

  # # Response to the "Like" action.
  # # This method should create a like relation. And update the
  # # "Like" button to "Unlike"
  # # @param current_user, event_id
  # # @return JSON/HTML
  # def create_like
  #
  # end
  #
  # # Response to the "Unlike" action.
  # # This method should destroy a like relation. And update the
  # # "Unlike" button to "like"
  # # @param current_user, event_id
  # # @return JSON/HTML
  # def destroy_like
  #
  # end

  private
    def set_services
      @society_service = SocietiesService.new
      @user_service = UsersService.new
      @event_service = EventsService.new
      @ticket_service = TicketsService.new
    end

    def login_params
      params.permit(:email, :password, :institution)
    end

    def flag_params
      params.permit(:id, :type)
    end

end