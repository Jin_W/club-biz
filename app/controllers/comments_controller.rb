class CommentsController < ApplicationController

  def create
    @comment_hash = comment_params.merge(user_id: current_user.id)
    @obj = @comment_hash[:commentable_type].constantize.find(@comment_hash[:commentable_id])
    # Not implemented: check to see whether the user has permission to create a comment on this object
    @comment = @obj.comments.new(@comment_hash)
    if @comment.save
      render :partial => "comments/comment", :locals => { :comment => @comment }, :layout => false, :status => :created
    else
      flash[:error] = 'save comments fail.'
    end
  end

  def comment_params
    params.permit(:comment, :commentable_id, :commentable_type)
  end

end