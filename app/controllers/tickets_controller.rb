class TicketsController < ApplicationController
  before_filter :set_services


  # @param ticket_id
  # @return HTML

  def request_tickets
    @tickets = @event_service.find_event_tickets(params[:event_id])
    respond_to do |format|
      #format.json {render :json => @tickets}
      #format.html {render :partial=>"shared/ticket_page", :locals => {:tickets =>@tickets}}
      format.js
    end
  end

  # @param ticket_id, num
  # @return HTML
  def reserve
    if @ticket_service.reserve_ticket(params[:id], params[:num])
      @user_service.create_user_ticket(current_user.id, params[:id], params[:num])
      render partial: 'shared/alert', locals: { title: 'Success: ', message: 'Tickets have been reserved successfully!'}
    else
      render partial: 'shared/alert', locals: { title: 'Error: ', message: 'Something wrong when reserving tickets!'}
    end
    # respond_to do |format|
    #   format.json render
    # end
  end

  # @param
  # @return HTML
  def buy

  end



 private
  def set_services
    @event_service = EventsService.new
    @ticket_service = TicketsService.new
    @user_service = UsersService.new
  end
end