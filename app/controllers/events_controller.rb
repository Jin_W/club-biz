require "prawn"

class EventsController < ApplicationController
  before_filter :set_services
  # Get the form ready for the event registration.
  # Should check the authorization of current user.
  # @param society_id
  # @return new_event_page
  def new
    @event = @event_service.new_event
    @event.event_images.build
    @event.tickets.build
    @society_id = params[:society_id]
  end

  # Create the new event for current society.
  # Should check the authorization and information format.
  # @param current_society, params{name, time, venue, type, short_des, long_desc}
  # @return event_view_page
  def create
  # TODO
  # Should check if current user is the admin of the society

  # Create Event using the parameters
  event_time        = Time.new(event_params['time(1i)'], event_params['time(2i)'], event_params['time(3i)'],
                      event_params['time(4i)'], event_params['time(5i)'], event_params['time(6i)'])
  new_event_params  = event_params.slice(:name, :society_id, :venue, :event_type,
                      :short_description, :long_description).merge(:time => event_time)
  event             = @event_service.create_event(new_event_params)

  # Create the event image from the uploader
  @event_service.create_event_image_from_upload event.id, event_params[:event_image_id]

  # Create the event tickets
  event_params[:tickets_attributes].each_value do |ticket_attribute|
    @ticket_service.create_ticket ticket_attribute.merge(:event_id => event.id)
  end

  # Create the event invitation
  # TODO
  # Should change the link between Society - Invitation to Event - Invitation
  # And move the invitation related methods from Society to Event
  event_params[:invitations].to_s.split(',').each do |society_id|
    @event_service.create_invitation({:event_id   => event.id,
                                      :society_id => society_id})
  end

  redirect_to root_path

  end # create

  # Show the basic info of the event. Include:
  # • The posters of the event.
  # • The name, time, venue, short_desc of the event.
  # • The icon image of the event/society?
  # should also record the poster number.
  # @param event_id
  # @return event_view_page
  def show

    event = @event_service.find_event(params[:id])
    images = @event_service.find_event_images(params[:id])
    @viewer_item = event
    @viewer_images = images
    @flag = Hash.new
    @flag[:state] = @user_service.like_event? current_user.id, params[:id]
    @flag[:type] = 'event'
    @flag[:id] = params[:id]

    respond_to do |format|
      format.js
    end

  end

  # Edit the information of the event. Ticket info cannot be changed.
  # Should return the form contain the current information.
  # @param event_id
  # @return edit_event_page
  def edit

  end

  # Update the information of existing event.
  # Should check the information.
  # @param event_id
  # @return event_view_page
  def update

  end

  # Get the report of current event. These info includes:
  # • The society that the event belongs to.
  # • The event detail information. Name, time, venue...
  # • The event shared societies.
  # • The tickets info. Total and Sales.
  # These may not include all the images of the events.
  # @param event_id
  # @return event_report_page : HTML/PDF?
  def report
    event = @event_service.find_event params[:event_id]
    society = @event_service.find_event_society(event.id)
    tickets = @event_service.find_event_tickets event.id
    event_images = @event_service.find_event_images event.id
    invited_societies = @event_service.find_event_invited_societies event.id

    file_name = "#{ event.name.downcase.delete(' ') }-report.pdf"

    file_path = Rails.root.join('public', 'pdf', file_name)

    Prawn::Document.generate(file_path) do
      font_size(25) do
        font "Times-Roman", :style => :bold
        pad_bottom(20) do
          text event.name + " - Report"
        end
      end

      stroke_horizontal_rule

      move_down 10
      default_leading 5

      font "Times-Roman", :style => :normal

      font_size(20) do
        text 'Event Details',
             :color => "22A7F0"
      end

      font_size 14
      text '       Name: ' + event.name
      text '       Time: ' + event.time.to_s
      text '      Venue: ' + event.venue
      text 'Description: ' + event.long_description
      move_down 10

      text 'Tickets Information Table: ', :align => :center
      data = [["Ticket Type", "Ticket Price", "Total Number", "Rest Number"]]

      tickets.each do |ticket|
        data += [[ticket.ticket_type, ticket.price.to_s, ticket.total_num.to_s, ticket.rest_num.to_s]]
      end

      table(data, :row_colors => (["6BB9F0"] + ["C5EFF7"] * tickets.length), :position => :center)

      move_down 10

      image 'public/' + event_images.first.event_image.url(:preview), :fit => [200, 200], :position => :center

      move_down 10

      stroke_horizontal_rule

      move_down 10

      font_size(20) do
        text 'Society Details',
             :color => "22A7F0"
      end

      font_size 14
      text 'Name: ' + society.name
      text 'Description: ' + society.description
      text 'Website: ' + society.website_link.to_s

      font_size(20) do
        text 'Invitations',
             :color => "22A7F0"
      end
      string = ""
      invited_societies.each do |invited_soc|
        string += "<color rgb='#{ "%06x" % (rand * 0xffffff) }'>" + invited_soc.name + "</color>  "
      end
      text string, :inline_format => true
    end

    respond_to do |format|
      format.pdf do
        send_file(file_path, :type => 'application/pdf', :disposition => 'inline')
      end
    end

  end

  # Get the detail of current event. These info includes:
  # • The detailed long_description of the event.
  # • The ticket information/button.
  # • The comments button.
  # • The like/unlike button.
  # • (option)The report button.
  # • (option)Edit button.
  # • (option)Delete button.
  # @param event_id
  # @return event_detail_page : HTML/JSON
  def detail

  end

  # Show the recent comments of current event.
  # @param event_id
  # @return event_comment_page : JSON
  def show_comments
    @event = @event_service.find_event(params[:event_id])

    # TODO
    # should put into service
    @comments = @event.comments.recent.limit(5)
    @new_comment = @event.comments.build

    render 'events/show_comments'
  end

  def event_image_upload
    uploader = EventImageUploader.new
    uploader.cache! event_image_params[:event_images_attributes]['0'][:event_image]
    # File.open(Rails.root.join('public', file_path), 'wb') do |file|
    #   rnt = file.write(tempfile.read)
    respond_to do |format|
      if uploader.cached?
        # format.html { redirect_to @upload_photo, notice: 'Upload photo was successfully created.' }
        # format.json { render action: 'show', status: :created, location: @upload_photo }
        format.json { render json: {files: [to_jq_preview(uploader)]}, status: :created, location: @upload }
      else
        # format.html { render action: 'new' }
        format.json { render json: 'error', status: :unprocessable_entity }
      end
    end
    # end
    # @event_image = @event_service.create_event_image({event_id: 5, event_image: @tempfile})
  end

  private
    def set_services
      @event_service  = EventsService.new
      @ticket_service = TicketsService.new
      @user_service = UsersService.new
    end

    def event_params
      params.require(:event).permit(:name,
                                    :society_id,
                                    :time,
                                    :venue,
                                    :event_type,
                                    :short_description,
                                    :long_description,
                                    event_images_attributes: [:event_image],
                                    tickets_attributes: [:ticket_type, :total_num, :price]
      ).merge(:event_image_id => params[:event_image_id])
       .merge(:invitations => params[:invitations])

    end

    def event_image_params
      params.require(:event).permit(event_images_attributes: [:event_image] )
    end


end