class SocietiesController < ApplicationController
  before_filter :set_services

  # @param
  # @return HTML
  def new
    @society = @society_service.new_society
  end

  # @param
  # @return HTML
  def index

  end

  # @param
  # @return HTML
  def edit

  end

  # @param
  # @return HTML
  def update

  end

  # @param
  # @return HTML
  def create

    puts society_params[:treasurer_email]

    # People
    if society_params[:my_position] == 1.to_s
      president_id = current_user.id
      treasurer_id = @user_service.find_or_create_user('', society_params[:treasurer_email], current_user.institution_id).id
    else
      president_id = @user_service.find_or_create_user('', society_params[:president_email], current_user.institution_id).id
      treasurer_id = current_user.id
    end

    # Create Society
    new_society    = @society_service.create_society(society_params.slice(:name,
                                                                          :registration_number,
                                                                          :contact_number,
                                                                          :website_link,
                                                                          :description)
                                                                  .merge({:president_id   => president_id,
                                                                          :treasurer_id   => treasurer_id,
                                                                          :admin_id       => current_user.id,
                                                                          :contact_email  => current_user.email,
                                                                          :state          => false,
                                                                          :institution_id => current_user.institution_id}))

    @society_service.create_society_image(new_society.id, ActionController::Base.helpers.asset_path('default_portrait.jpg'))

    # Create Membership
    @society_service.create_member new_society.id, president_id, 'president'
    @society_service.create_member new_society.id, treasurer_id, 'treasurer'

    if new_society
      redirect_to root_path
    else
      redirect_to new_societies_path
    end

  end

  # @param
  # @return HTML
  def unapproved

  end

  # @param society_id
  # @return HTML
  def approve

  end

  # @param
  # @return HTML
  def show
    society = @society_service.find_society(params[:id])
    images = @society_service.find_society_image(params[:id])
    @viewer_item = society
    @viewer_images = images
    @flag = Hash.new
    @flag[:state] = @user_service.follow_society? current_user.id, params[:id]
    @flag[:type] = 'society'
    @flag[:id] = params[:id]

    render 'events/show'
  end

  # @param
  # @return HTML
  def detail

  end

  # @param
  # @return HTML
  def create_img_desc

  end

  # @param
  # @return HTML
  def update_img_desc

  end

  # @param
  # @return HTML
  def new_img_desc

  end

  # @param
  # @return HTML
  def new_member

  end

  # @param
  # @return HTML
  def create_member

  end

  # @param member_id
  # @return HTML
  def edit_member

  end

  # @param
  # @return HTML
  def update_member

  end

  # @param member_id
  # @return HTML
  def destroy_member

  end

  # @param
  # @return HTML
  def show_members

  end

  # @param
  # @return HTML
  def show_events
    @events = @society_service.find_society_events params[:id]
    accepted_events = @society_service.find_society_accepted_inv params[:id]
    unaccepted_events = @society_service.find_society_unaccepted_inv params[:id]
    render 'societies/show_events', locals: {accepted_events: accepted_events, unaccepted_events: unaccepted_events}
  end

  private
    def set_services
      @society_service = SocietiesService.new
      @user_service = UsersService.new
    end

    def society_params
      params.require(:society).permit(:name, :registration_number, :website_link, :description)
      .merge(:my_position => params[:my_position])
      .merge(:president_email => params[:president_email])
      .merge(:treasurer_email => params[:treasurer_email])
      .merge(:contact_number => params[:contact_number])
      .merge(:can_edit => params[:can_edit])
    end
end