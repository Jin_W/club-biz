var search_global = function() {

    var submit_form = $("#search-form");

    submit_form.on('submit', function(e) {
        e.preventDefault(); //Will prevent the submit...
        //Add additional code here
        var search_input = submit_form.find('#keyword');
        search_input.addClass("loading"); // show the spinner
        // grab the form wrapping the search bar.
        var checked_options = $('.dropdown-menu').find('input:checked');
        var filter = {};
        checked_options.each(function(index, element) {
            filter[element.value] = 1;
        });
        var url = "/search"; // live_search action.
        var keyword = search_input.val(); // grab the data in the form
        var timeline = $("#center");
        $.get(
            url,
            $.extend({
                keyword: keyword
            }, filter),
            function (html) { // perform an AJAX get
                search_input.removeClass("loading"); // hide the spinner
                timeline.html(html); // replace the "results" div with the results
            });
    });

//    submit_form.find('button').on("click", function() {
//
//    });

    $('.dropdown-menu').click(function(e) {
        e.stopPropagation();
    });


};

$(document).on('ready page:load', search_global);

