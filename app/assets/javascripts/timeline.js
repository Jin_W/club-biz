var lg_window = 1200;
var md_window = 992;
var sm_window = 768;
var xs_window = 478; // or 640


var timeline_init = function(size) {

    var timelineInvertedItems = $('li.timeline-right');
    var i;
    var timelinePanels = $('.timeline-panel');
    var timelineBadges = $('.timeline-badge');

    if ($(window).width() > size) {
        for (i = 0; i < timelineInvertedItems.size(); i ++) {
            timelineInvertedItems[i].classList.add('timeline-inverted');
        }
        $('.timeline')[0].classList.remove('change');
        for (i = 0; i < timelinePanels.size(); i ++) {
            timelinePanels[i].style.width = "46%";
        }
        for (i = 0; i < timelineBadges.size(); i ++) {
            timelineBadges[i].style.left = "50%";
        }
    }
    else {
        for (i = 0; i < timelineInvertedItems.size(); i ++) {
            timelineInvertedItems[i].classList.remove('timeline-inverted');
        }
        $('.timeline')[0].classList.add('change');
        for (i = 0; i < timelinePanels.size(); i ++) {
            timelinePanels[i].style.width = "95%";
        }
        for (i = 0; i < timelineBadges.size(); i ++) {
            timelineBadges[i].style.left = "99%";
        }

    }


};




$(document).on('ready page:load', function() {
    timeline_init(xs_window);
});

window.onresize = function(event) {
    if ($('#right-viewer').hasClass('hidden')) {
        timeline_init(xs_window);
    }
};

