var society_registration_initial = function() {
    society_reg_form_validation();
    change_role();
};

var society_reg_form_validation = function() {
    $('#new_society').bootstrapValidator({
        excluded: ':disabled, :hidden, :not(:visible)',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        live: 'enabled',
        message: 'This value is not valid',
        submitButtons: 'input[type="submit"]',
        submitHandler: null,
        trigger: null,
        fields: {
            society_name: {
                message: 'This value is not valid',
                selector: '#society_name',
                validators: {
                    notEmpty: {
                        message: 'Society Name cannot be empty'
                    },
                    regexp: {
                        regexp: /^[a-zA-Z0-9_ ]+$/,
                        message: 'The society name can only consist of alphabetical, number and underscore'
                    }
                }
            },
            society_registration_number: {
                message: 'This value is not valid',
                selector: '#society_registration_number',
                validators: {
                    notEmpty: {
                        message: 'Society Registration Number cannot be empty'
                    },
                    regexp: {
                        regexp: /^[0-9]+$/,
                        message: 'The number can only consist of numbers'
                    }
                }
            }
        }
    });
};

var change_role = function() {
    var president_email_input = $('#president-email-div');
    var treasurer_email_input = $('#treasurer-email-div');
    var select_role_list      = $('#my-position');
    select_role_list.change(function() {
        if (select_role_list.val() == 1) {
            president_email_input.addClass('hidden');
            treasurer_email_input.removeClass('hidden');
        }
        else {
            president_email_input.removeClass('hidden');
            treasurer_email_input.addClass('hidden');
        }
    });
};

$(document).on('ready page:load', society_registration_initial());