var event_registration_initial = function() {
    fileupload();
    search_submit();
};

var fileupload = function() {
    $('#event_image_upload').fileupload({
        url: '/event_image_upload',
        dataType: 'json',
        done: function(e, data) {
            $.each(data.result.files, function(index, file) {
                list = $('#preview > .hide').clone();
                list.find('> a').attr('href', file.url);
                list.find('img').attr('src', file.preview_url);
//                list.find('h4').text(file.name);
                list.find('#event_image_id').attr('value', file.name);
                list.find('.media-body a').attr('data-method', '').attr('href', file.preview_url)
                    .attr('data-remote', true);
                list.removeClass('hide').appendTo('#preview');
            });
        },
        progressall: function(e, data) {
            var progress = parseInt(data.loaded / data.total * 100, 10);
            $('.progress .progress-bar').css('width', progress + '%');
        }
    });
};

var search_submit = function() {
    var search_box = $("#search-box");
    search_box.bind("keyup", function(ev) {
        $(this).addClass("loading"); // show the spinner
        // grab the form wrapping the search bar.
        var url = "/search?society=1"; // live_search action.
        var keyword = search_box.val(); // grab the data in the form
        $.get(url, { keyword: keyword }, function (html) { // perform an AJAX get
            $("#search-box").removeClass("loading"); // hide the spinner
            $("#live-search-results").html(html); // replace the "results" div with the results
            create_invitation();
        });
    });
};

var create_invitation = function() {

    var society_list = $('#live-search-results');
    var invitation_list = $('#invitation-list');
    society_list.find('.list-group .list-group-item').each(function(index, element) {
        $(element).attr('href', '#');
        $(element).click(function() {
            console.log('clicked');
            var id = $(this).data('society-id');
            if (update_invitation(id, 'add')) {
                var invitation = invitation_list.find('span').first().clone();
                invitation.text($(this).find('h4').text());
                invitation.data('id', id);
                invitation.tooltip();
                invitation.removeClass('hidden').appendTo('#invitation-list');
                invitation.click(function () {
                    delete_invitation($(this));
                });
            }
        });
    });
};

var update_invitation = function(id, action) {
    var invitations = $('#invitation-list').find('#invitations');
    var current_list = invitations.val().split(',').filter(function(v) {return v != ''});
    var index = jQuery.inArray(id.toString(), current_list);

    if (index == -1 && action == 'add') {
        current_list.push(id.toString());
        invitations.val(current_list.join(','));
        return true;
    }
    else if (index >= 0 && action == 'remove') {
        current_list.splice(index, 1);
        invitations.val(current_list.join(','));
        return true;
    }
    else {
        return false;
    }

};

var delete_invitation = function(item) {
    update_invitation(item.data('id'), 'remove');
    item.tooltip('hide');
    item.remove();
};

$(document).on('ready page:load', event_registration_initial());