var tickets_reserve = function() {
    var input_groups = $('#ticket-info').find('.input-group');
    input_groups.each(function(index, element) {
        $(element).find('button').on('click', function() {
            var input_box = $(element).find('input');
            $.ajax({
                type: 'POST',
                url: '/ticket_reserve',
                data: {num: input_box.val(), id: input_box.data('ticket-id')}
            }).done(function(html) {
                $('#alert-banner').html(html);
                input_box.val('');
            });
        });
    });
};

tickets_reserve();