/* off-canvas sidebar toggle */

//$(document).ready(function() {
var sidebar = function() {

    var row = $('.row-offcanvas');
    var sidebar = $('#sidebar');


    $('[data-toggle=offcanvas]').on('click', function () {
        $(this).toggleClass('visible-md text-center');
        $(this).find('i').toggleClass('glyphicon-chevron-right glyphicon-chevron-left');
        row.toggleClass('active');
        $('.lg-sidebar').toggleClass('hidden-md').toggleClass('visible-md');
        $('.md-sidebar').toggleClass('visible-md').toggleClass('hidden-md');
        $('#center').toggleClass('active');
        //    $('#btnShow').toggle();
//        alert("OK");
    });

    $('#sidebar-toggle-button').on('click', function() {

        row.toggleClass('active');
        sidebar.toggleClass('scroll');
        sidebar.toggleClass('hidden-sm hidden-xs');
    });

    swipe_action();

};

$current_role_index = 0;

var swipe_action = function() {

    var sidebar = $('#sidebar');
    var max_role_index = sidebar.find('#swipable').data('roleNum') - 1;

    $('.role-switch-left').on('click', function(ev) {swipe_right(ev)});

    $('.role-switch-right').on('click', function(ev) {swipe_left(ev)});

    Hammer(sidebar).on('swipeleft dragleft', function(ev) {swipe_left(ev)});

    Hammer(sidebar).on('swiperight dragright', function(ev) {swipe_right(ev)});


    var init = function() {
        for (var i = 0; i < max_role_index; i ++) {
            if (i == $current_role_index) {
                $('#swipable-item' + i.to_s).removeClass('hidden');
            }
            else {
                $('#swipable-item' + i.to_s).addClass('hidden');
            }
        }
    };

    var swipe_left = function(ev) {
        if ($current_role_index < max_role_index) {
            var swipable_item_now = '#swipable-item' + String($current_role_index);
            var swipable_item_then = '#swipable-item' + String($current_role_index + 1);
            $(swipable_item_now).addClass('hidden');
            $(swipable_item_then).removeClass('hidden');
            $current_role_index += 1;
        }
        if (ev.type == 'swipeleft' || ev.type == 'dragleft' ) {
            ev.gesture.stopDetect();
        }
    };

    var swipe_right = function(ev) {
        if ($current_role_index > 0) {
            var swipable_item_now = '#swipable-item' + String($current_role_index);
            var swipable_item_then = '#swipable-item' + String($current_role_index - 1);
            $(swipable_item_now).addClass('hidden');
            $(swipable_item_then).removeClass('hidden');
            $current_role_index -= 1;
        }
        if (ev.type == 'swipeleft' || ev.type == 'dragleft' ) {
            ev.gesture.stopDetect();
        }
    };
};




$(document).on('ready page:load', sidebar);

$(document).on('page:change', function() {
    $current_role_index = 0;
});