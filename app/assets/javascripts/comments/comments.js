var comment_form_submit = function() {
    var comment_form = $('#new_comment');

    comment_form.on('ajax:beforeSend', function() {
            $(this).find('#comment').attr('disabled', 'disabled');
    });

    comment_form.on('ajax:success', function(data, xhr) {
            $(this).find('#comment').removeAttr('disabled', 'disabled').val('');
            $(xhr).hide().insertAfter($(this)).show('slow');
    });
};

$(document).on('ready page:load', comment_form_submit());