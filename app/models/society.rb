class Society < ActiveRecord::Base

  has_many :events
  has_one :society_image

  belongs_to :institution
end
