class Event < ActiveRecord::Base
  acts_as_commentable

  belongs_to :society

  has_many :event_images
  has_many :tickets

  accepts_nested_attributes_for :event_images
  accepts_nested_attributes_for :tickets

end
