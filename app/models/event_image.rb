class EventImage < ActiveRecord::Base
  belongs_to :event
  mount_uploader :event_image, EventImageUploader
end
