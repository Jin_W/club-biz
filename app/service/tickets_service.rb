class TicketsService
  # @param
  # @return Ticket
  def new_ticket

  end

  # Model will check validation
  # @param
  # @return ticket_id
  def create_ticket params
    Ticket.create params.merge(:rest_num => params[:total_num])
  end

  # @param
  # @return Boolean
  def reserve_ticket ticket_id, num
    ticket = Ticket.find(ticket_id)
    return false if ticket.rest_num<num.to_i


    unless ticket.blank?
      ticket.update_attribute(:rest_num, ticket.rest_num - num.to_i)
      true
    end
  end

  # @param ticket_id
  # @return Ticket
  def find_ticket ticket_id
    Ticket.find(ticket_id)
  end

  # @param ticket_id
  # @return Event
  def find_ticket_event ticket_id
    ticket = Ticket.find(ticket_id)
    Event.find(ticket.event_id)
  end

  # @param user_id,ticket_id
  # @return Boolean
  def check_membership user_id, ticket_id
     true
  end
end