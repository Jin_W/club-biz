class SocietiesService
  # @param
  # @return Society
  def new_society
    Society.new
  end

  # Model will check validation
  # @param  params
  # @return society_id
  def create_society params
    # should select the params
    Society.create params
  end

  # @param
  # @return Boolean
  def update_society society_id, params
    society = Society.find(society_id)
    unless society.blank?
      society.update!(params)
    end
  end

  # @param society_id,content
  # @return Boolean
  def update_society_description society_id,content
  #   Not applicable now
  end

  # @param society_id
  # @return Society
  def find_society society_id
    Society.find(society_id)
  end

  # @param value,num
  # @return Society[]
  def find_societies_by_state value, num
    Society.where(state: value).limit(num)
  end

  # @param keyword
  # @return Society[]
  def find_societies_by_name keyword
    Society.where("lower(name) like ?", "%#{keyword.downcase}%")
  end

  # @param society_id
  # @return Member[]
  def find_society_members society_id
    Member.where(society_id: society_id)
  end

  # get the portrait of society
  # @param society_id
  # @return SocietyImage
  def find_society_image society_id
    SocietyImage.where(society_id: society_id)
  end

  # @param society_id
  # @return Event[]
  def find_society_events society_id, num = 10
    Event.where(society_id: society_id)
  end

  def find_society_accepted_inv society_id
    events = []
    Invitation.where(society_id: society_id).each do |invitation|
      events << Event.find(invitation.event_id) if invitation.accept
    end
    events
  end

  def find_society_unaccepted_inv society_id
    events = []
    Invitation.where(society_id: society_id).each do |invitation|
      events << Event.find(invitation.event_id) unless invitation.accept
    end
    events
  end

  # @param society_id,user_id,role
  # @return member_id[]
  def create_member society_id,user_id,role
    Member.create({society_id: society_id,
                   user_id: user_id,
                   role: role})
  end

  # @param member_id
  # @return Member
  def find_member member_id
    Member.find member_id
  end

  # @param params
  # @return Boolean
  def update_member params

  end

  # @param
  # @return Member
  def new_member

  end

  # @param member_id
  # @return Boolean
  def destroy_member member_id

  end

  # @param society_id,link
  # @return society_image_id
  def create_society_image society_id,link
    SocietyImage.create({society_id: society_id, link: link})
  end

  def find_institution_by_name name
    Institution.where(name: name).first
  end

  def find_institution id
    Institution.find(id)
  end

end