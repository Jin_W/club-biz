class EventsService

  # Get a instance of Event. can be used for form generation.
  # @param
  # @return Event
  def new_event params = nil
    params.nil? ? Event.new : Event.new(params)
  end

  def new_event_image params = nil
    params.nil? ? EventImage.new : EventImage.new(params)
  end

  # Create an event. Need to check duplication and the length?(In model). Note: Since
  # creating an event including creating relevant event_images, tickets and invitations,
  # these are all sort of business logic. (domain relations)
  # @param name
  # @param society_id
  # @param time
  # @param venue
  # @param event_type
  # @param short_description
  # @param long_description
  # @return Event
  def create_event params
  #   should select the params
    Event.create params
  end

  def create_event_image params
    EventImage.create params
  end

  def create_event_image_from_upload event_id, event_image_id
    new_image = create_event_image({event_id: event_id})
    uploader  = EventImageUploader.new(new_image, :event_image)
    uploader.retrieve_from_cache!(event_image_id)
    uploader.store!
    new_image.update_attribute(:event_image, uploader.file)
  end

  # Update an event. Need to check the length?(In model).
  # @param name
  # @param society_id
  # @param time
  # @param venue
  # @param event_type
  # @param short_description
  # @param long_description
  # @return Boolean
  def update_event params

  end

  # Delete an event.
  # @param event_id
  # @return Boolean
  def destroy_event event_id

  end

  # Find an event by name.
  # @param name
  # @return Event[]
  def find_events_by_name name
    Event.where("lower(name) like ?", "%#{name.downcase}%")
  end

  # Find an event by venue.
  # @param venue
  # @return Event[]
  def find_events_by_venue venue
    Event.where("lower(venue) like ?", "%#{venue.downcase}%")
  end

  # Find the society of an event.
  # @param event_id
  # @return Society
  def find_event_society event_id
    Event.find(event_id).society
  end

  # Find the event by event_id
  # @param event_id
  # @return Event
  def find_event event_id
    Event.find(event_id)
  end

  # Find the tickets of an event.
  # @param event_id
  # @return Ticket[]
  def find_event_tickets event_id
    Ticket.where(event_id: event_id).order("ticket_type")
  end

  # @param event_id,society_id
  # @return invitation_id
  def create_invitation params
    Invitation.create params.merge(:accept => false)
  end

  # @param invitation_id,event_id,society_id
  # @return Boolean
  def update_invitation invitation_id,event_id,society_id

  end

  def find_event_invited_societies event_id
    societies = []
    invitations = Invitation.where(event_id: event_id)
    invitations.each do |invitation|
      societies << Society.find(invitation.society_id)
    end
    societies
  end

  def find_event_images event_id
    EventImage.where(event_id: event_id)
  end

end