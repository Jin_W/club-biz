require 'net/http'

class UsersService

  # Check if the user's information is correct or not.
  # This methods should send the information to external link and get return value.
  # @param institution_id
  # @param email
  # @param password
  # @return boolean
  def user_login_validation oauth_link, email, password
    host = oauth_link[0..(oauth_link.index(':') - 1)]

    port = oauth_link[(oauth_link.index(':') + 1)..(oauth_link.index('/') - 1)].to_i

    subattr = oauth_link[oauth_link.index('/')..(oauth_link.length - 1)]

    http = Net::HTTP.new(host, port)

    response = http.get(subattr + '?' + {email: email, password: password}.to_param)

    # res = Net::HTTP::get_response(oauth_link, {email: login_params[:email], password: login_params[:password]})

    unless response.blank? || response.body.blank?
      json_user = JSON.parse(response.body)['user']
      unless json_user.blank? || !json_user.include?('name')
        JSON.parse(json_user)
      end
    end
  end

  # Find or Create User. This should be used in user login or society member creation, edition process.
  # @param name
  # @param email
  # @param institution_id
  # @return user
  def find_or_create_user name, email, institution_id
    new_user = User.where({email: email, institution_id: institution_id}).first_or_create do |u|
      u.name = name
      u.email = email
      u.portrait_link = ActionController::Base.helpers.asset_path('default_portrait.jpg')
      u.institution_id = institution_id
    end

    if new_user.name.blank?
      new_user.update_attribute(:name, name)
    end

    new_user
  end


  # NOT APPLICABLE NOW
  def create_user name, email, portrait_link, institution_id
    raise "NO IMPLEMENTATION! - UsersService - create_user()"
  end

  # Update the portrait_link of user.
  # @param portrait_link
  # @return portrait_link
  def update_user_portrait portrait_link

  end

  # Create a notification for a certain user.
  # @param user_id
  # @param content
  # @return UserNotification_id
  def create_user_notification user_id, content

  end

  # Get all the notifications a user has.
  # @param user_id
  # @return UserNotification[]
  def find_user_notifications user_id

  end

  # Get all the societies that a user joins in currently.
  # @param user_id
  # @return Society[]
  def find_user_joining_societies user_id, num = 10
    societies = []
    members = Member.where(user_id: user_id).limit(num)
    members.each do |member|
      societies << Society.find(member.society_id)
    end
    societies
  end

  # Get all the societies that a user follows currently.
  # @param user_id
  # @return Society[]
  def find_user_following_societies user_id, num = 10
    societies = []
    follows = Follow.where(user_id: user_id).limit(num)
    follows.each do |follow|
      societies << Society.find(follow.society_id)
    end
      societies
  end

  # Get all the events that a user likes currently.
  # @param user_id
  # @return event[]
  def find_user_liking_events user_id, num = 10
    events = []
    likes = Like.where(user_id: user_id).limit(num)
    likes.each do |like|
      events << Event.find(like.event_id)
    end
    events
  end


  def find_user_booked_events user_id
    events = []
    find_user_tickets(user_id).each do |user_ticket|
      events << Event.find(Ticket.find(user_ticket.ticket_id).event_id)
    end
    events
  end

  # Get all the tickets that a user has. This includes both the reserved and bought ones.
  # @param user_id
  # @return ticket[]
  def find_user_tickets user_id
    UserTicket.where(user_id: user_id)
  end

  # Create a follow relation between a user and a society. Should check the duplication.
  # @param user_id
  # @param society_id
  # @return Follow
  def create_follow user_id, society_id
    Follow.create({user_id: user_id,
                   society_id: society_id})
  end

  # Delete a follow relation between a user and a society. Should check if exist.
  # @param user_id
  # @param society_id
  # @return Boolean
  def destroy_follow user_id, society_id
    Follow.where({user_id: user_id, society_id: society_id}).first.destroy
  end

  # Create a like relation between a user and an event. Should check the duplication.
  # @param user_id
  # @param event_id
  # @return Like
  def create_like user_id, event_id
    Like.create({user_id: user_id,
                 event_id: event_id})
  end

  # Delete a like relation between a user and an event. Should check if exist.
  # @param user_id
  # @param event_id
  # @return Boolean
  def destroy_like user_id, event_id
    Like.where({user_id: user_id, event_id: event_id}).first.destroy
  end

  # Create a user ticket. After reservation. State of the ticket should be unpaid(false) by default.
  # @param user_id
  # @param ticket_id
  # @param num
  # @return UserTicket_id
  def create_user_ticket user_id, ticket_id, num
    UserTicket.create({user_id: user_id,
                       ticket_id: ticket_id,
                       number: num.to_i,
                       state: false})
  end

  def like_event? user_id, event_id
    Like.exists?(user_id: user_id, event_id: event_id)
  end

  def follow_society? user_id, society_id
    Follow.exists?(user_id: user_id, society_id: society_id)
  end

end