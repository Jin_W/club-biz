# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)


institutions = Institution.create([{ name: 'the University of Melbourne', admin_id: 1, auth_link: '127.0.0.1' },
                   { name: 'the University of Monash', admin_id: 1, auth_link: '127.0.0.1' }])

users = User.create([{ name: 'Jin Wang', email: 'jinw1990@gmail.com', portrait_link: 'http://wecorn.com/files/note/22/lens.png', institution_id: institutions.first.id },
                     { name: 'Wentao Li', email: 'ljl884@gmail.com', portrait_link: 'http://wecorn.com/files/note/23/volume.png', institution_id: institutions.first.id },
                     { name: 'Michael Owen', email: 'jinw2@student.unimelb.edu.au', portrait_link: 'http://wecorn.com/files/note/24/flash.png', institution_id: institutions.first.id },
                     { name: 'James Bone', email: 'owenwj@sina.com', portrait_link: 'http://wecorn.com/files/note/25/paintroller.png', institution_id: institutions.first.id }])
2.times do |i|
  Society.create([{ name: "MusicBuddy ##{i}", president_id: users.first.id, treasurer_id: users.last.id, admin_id: User.find(i + 1).id,
                              description: "##{i} We are a group of music fans. Join us to enjoy the BEST music in the World.", contact_number: '0412813212',
                              contact_email: 'owenwjowenwj@gmail.com', state: true, institution_id: institutions.first.id }])
end

5.times do |i|
  Event.create([{ name: "Friday Night ##{i} Season", society_id: Society.first.id, time: (i + 1).days.from_now, venue: '121 Wills St.', event_type: 'Music',
                  short_description: "Music night ##{i}. come to join us and enjoy Pop music all over the night.",
                  long_description: 'On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by
the charms of pleasure of the moment, so blinded by desire, that they cannot foresee the pain and trouble that are bound to ensue; and equal blame belongs
to those who fail in their duty through weakness of will, which is the same as saying through shrinking from toil and pain. These cases are perfectly simple
and easy to distinguish. In a free hour, when our power of choice is untrammelled and when nothing prevents our being able to do what we like best, every pleasure
is to be welcomed and every pain avoided. But in certain circumstances and owing to the claims of duty or the obligations of business it will frequently occur that
pleasures have to be repudiated and annoyances accepted. The wise man therefore always holds in these matters to this principle of selection: he rejects pleasures
to secure other greater pleasures, or else he endures pains to avoid worse pains.' }])
end


Member.create([{ :user => User.first, :society => Society.first, role: 'president' },
                         { user: User.find_by_name("Wentao Li"), society: Society.first, role: 'treasurer'},
                         { user: User.find_by_name('Michael Owen'), society: Society.first, role: 'member' }])
5.times do |i|
  EventImage.create([{ event_id: Event.find(i + 1).id, event_image: 'http://wecorn.com/files/note/26/10.jpg' }])
end

Follow.create([{ user_id: User.first.id, society_id: Society.first.id }])

Like.create([{ event_id: Event.first.id, user_id: User.first.id },
             { event_id: Event.find(2).id, user_id: User.last.id}])

Invitation.create([{ event_id: Event.first.id, society_id: Society.last.id, accept: false }])

SocietyImage.create([{ link: 'http://wecorn.com/files/note/27/Benevolent-Society-logo-RGB1.jpg', society_id: Society.first.id },
                     { link: 'http://wecorn.com/files/note/28/Unknown.jpg', society_id: Society.last.id }])

Ticket.create([{ event_id: Event.first.id, ticket_type: '1', price: 25.5, total_num: 100, rest_num: 31 },
               { event_id: Event.first.id, ticket_type: '2', price: 35.9, total_num: 10, rest_num: 5 },
               { event_id: Event.last.id, ticket_type: 'premium', price: 200, total_num: 5, rest_num: 5}])

10.times do |i|
  UserNotification.create([{ user_id: User.find(rand(1..3)).id, content: "Just a Notification ##{i}" }])
end

UserTicket.create([{ user_id: User.first.id, ticket_id: Ticket.first.id, number: 2, state: false },
                   { user_id: User.first.id, ticket_id: Ticket.last.id, number: 1, state: true }])


#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)