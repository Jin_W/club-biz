class CreateSocietyImages < ActiveRecord::Migration
  def change

    create_table :institutions do |t|
      t.string :name
      t.integer :admin_id
      t.string :auth_link
      t.timestamps
    end

    create_table :societies do |t|
      t.string :name
      t.integer :president_id
      t.integer :treasure_id
      t.integer :admin_id
      t.string :description
      t.string :contact_number
      t.string :contact_email
      t.boolean :state
      t.belongs_to :institutions
      t.timestamps
    end

    create_table :users do |t|
      t.string :name
      t.string :email
      t.string :portrait_link
      t.belongs_to :institutions
      t.timestamps
    end

    create_table :events do |t|
      t.string :name
      t.belongs_to :societies
      t.datetime :time
      t.string :venue
      t.string :type
      t.string :short_description
      t.string :long_description
      t.timestamps
    end

    create_table :society_images do |t|
      t.string :link
      t.belongs_to :societies
      t.timestamps
    end

    create_table :invitations do |t|
      t.integer :event_id
      t.integer :society_id
      t.boolean :accept
      t.timestamps
    end

    create_table :user_notifications do |t|
      t.belongs_to :users
      t.string :content
      t.timestamps
    end

    create_table :members do |t|
      t.integer :user_id
      t.integer :society_id
      t.string :role
      t.timestamps
    end

    create_table :likes do |t|
      t.integer :event_id
      t.integer :user_id
      t.timestamps
    end

    create_table :follows do |t|
      t.integer :user_id
      t.integer :society_id
      t.timestamps
    end

    create_table :user_tickets do |t|
      t.integer :user_id
      t.integer :ticket_id
      t.integer :number
      t.boolean :state
      t.timestamps
    end

    create_table :event_images do |t|
      t.belongs_to :events
      t.string :link
      t.timestamps
    end

    create_table :tickets do |t|
      t.belongs_to :events
      t.string :type
      t.float :price
      t.integer :total_num
      t.integer :rest_num
      t.timestamps
    end

  end
end
